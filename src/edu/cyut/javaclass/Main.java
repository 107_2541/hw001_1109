package edu.cyut.javaclass;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        // write your code here
                Scanner scanner = new Scanner(System.in);

                        String str = "abcdefgabcabc";
                System.out.println(str.replaceAll(".bc", "###"));

                       System.out.print("輸入手機號碼: ");
                str = scanner.next();

                      // 簡單格式驗證
               if(str.matches("[0-9]{4}-[0-9]{6}"))
                   System.out.println("格式正確");//當驗證手機號碼格式成功印出格式正確
               else
                   System.out.println("格式錯誤");//當驗證手機號碼格式失敗印出格式正確

                     System.out.print("輸入href標籤: ");
              // Scanner的next()方法是以空白為區隔
                     // 我們的輸入有空白，所以要執行兩次
                     str = scanner.next() + " " + scanner.next();

             // 驗證href標籤
                     if(str.matches("<a.+href*=*['\"]?.*?['\"]?.*?>"))
                  System.out.println("格式正確");//當驗證herf標籤成功印出格式正確
              else
                  System.out.println("格式錯誤");//當驗證herf標籤失敗印出格式錯誤

        System.out.print("輸入電子郵件: ");
               str = scanner.next();

                       // 驗證電子郵件格式
                             if(str.matches("^[_a-z0-9-]+([.][_a-z0-9-]+)*@[a-z0-9-]+([.][a-z0-9-]+)*$"))
                        System.out.println("格式正確");//當驗證電子郵件成功印出格式正確
              else
                    System.out.println("格式錯誤");//當驗證電子郵件失敗印出格式錯誤
    }
}
